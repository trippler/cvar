#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include "cvar.h"
#include "types.h"
void print_data(struct cvar_endpoint *data){
	switch(data->type){
		case INT64:
			printf("%"PRId64, ((struct cvar_endpoint_int64*)data)->value);
			break;
		case INT32:
			printf("%"PRId32, ((struct cvar_endpoint_int32*)data)->value);
			break;
		case STRING:
			for(size_t n = 0; 
			n < ((struct cvar_endpoint_string*)data)->length
				- sizeof(struct cvar_endpoint_string);
			n++)
				putchar(((struct cvar_endpoint_string*)data)->data[n]);
			break;
	}
}

void f(const char *name, void *data, void *pt){
	printf("\"%s\":\"", name);
	print_data(data);
	printf("\"\n");
}

int main(int argc, char* argv[]){
	const char* name = "test.cvar";
	if(argc == 2)
		name = argv[1];
	struct cvar_ctx *context = cvar_ctx_create();
	int64_t val = 0x7FFFFFFFFFFFFFFF;
	cvar_int64_set(context, "value", val);
	cvar_int64_set(context, "value2", 500L);
	cvar_int32_set(context, "32bit", 1337);
	cvar_string_set(context, "hello", "hello", 5);
	if(argc <= 1)
		cvar_file_save(context, name);
	struct cvar_ctx *context2 = cvar_ctx_create();
	cvar_file_load(context2, name);
	cvar_data_callback(context2, f, 0);
	cvar_ctx_destroy(context);
	cvar_ctx_destroy(context2);
}
