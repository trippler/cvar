#pragma once
#include <stdint.h>
#include <stdio.h>
#pragma pack(push)
#pragma pack(1)
enum cvar_type{
	INT64,
	INT32,
	STRING,
	UNBUFFERED_STRING
};

struct cvar_endpoint{
	enum cvar_type type:8;
};

struct cvar_endpoint_int64{
	struct cvar_endpoint parent;
	int64_t value;
};

struct cvar_endpoint_int32{
	struct cvar_endpoint parent;
	int32_t value;
};

struct cvar_endpoint_string{
	struct cvar_endpoint parent;
	int64_t length;
	char data[];
};
#pragma pack(pop)
/* Planned feature
struct cvar_endpoint_unbuf{
	struct cvar_endpoint parent;
	int64_t startb, stopb;
	FILE* filep;
};*/

