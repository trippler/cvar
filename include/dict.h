#pragma once
#define cvar_letter_range 256
#include <stdbool.h>
struct dict{
	struct dict *subs[cvar_letter_range];
	void *data;
};

struct dict* dict_create();
void dict_destroy(struct dict *d);
bool dict_data_set(struct dict *d, const char *path, void* data);
void* dict_data_get(struct dict *d, const char *path);
void dict_data_callback(struct dict *d, 
		void (*callback_function)(const char*,void*,void*), 
		void *passthrough);

