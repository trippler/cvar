#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include "dict.h"

/**
  * Internal struct used by cvar. This should never be accessed directly
  */
struct cvar_ctx{
	struct dict* d;
	int error;
};

/**
  * Creates a new cvar context
  */
struct cvar_ctx* cvar_ctx_create();

/**
  * Destroys a cvar context, including freeing all objects inside of it
  */
void cvar_ctx_destroy(struct cvar_ctx *context);

/**
  * Checks if the last operation in the cvar context had an error
  * Returns true if there was an error
  */
bool cvar_error(struct cvar_ctx *context);

/**
  * Exports all variables in the @context object to the @buffer. The size of
  * the buffer is writen to the @length object.
  * The buffer must be free'd by the caller.
  */
void cvar_ctx_buffer_export(struct cvar_ctx *context, char **buffer, int64_t *length);

/**
  * Imports all variables from the @buffer into the @context object
  */
void cvar_ctx_buffer_import(struct cvar_ctx *context, const char *buffer, int64_t length);

/**
  * Calls the @callback_function on every piece of data in the @context.
  * The @callback_function is expected to take the following parameters:
  * - const char* the full name of the current element
  * - void* a pointer to the cvar_endpoint_* element of the data
  * - void* the pointer passed along when cvar_data_callback was called.
  */
void cvar_data_callback(struct cvar_ctx *context, 
		void (*callback_function)(const char*,void*,void*), 
		void* passthrough);

/**
  * Gets the int64_t object from the cvar context at the @path specified.
  * If no object is found, the function returns 0 and error is set, therefore
  * one should always check the value of cvar_error after this operation.
  */
int64_t cvar_int64_get(struct cvar_ctx *context, const char *path);

/**
  * Gets the int32_t object from the cvar context at the @path specified.
  * If no object is found, the function returns 0 and error is set, therefore
  * one should always check the value of cvar_error after this operation.
  */
int32_t cvar_int32_get(struct cvar_ctx *context, const char *path);

/**
  * Sets the int64_t object of the cvar @context at @path to @value
  */
void cvar_int64_set(struct cvar_ctx *context, const char *path, int64_t value);

/**
  * Sets the int32_t object of the cvar @context at @path to @value
  */
void cvar_int32_set(struct cvar_ctx *context, const char *path, int32_t value);

/**
  * Gets the data at @path from the @context
  * The data is placed in the target pointed at by @buffer. If the target 
  * pointed at by@buffer is not NULL, that buffer is used with a max amount of 
  * data copied to it being @length bytes.
  *
  * If @buffer is NULL a new buffer is allocated to fit all the data.
  * @length will contain the amount of bytes in @buffer
  * @return will be a pointer to @buffer
  */
char* cvar_string_get(struct cvar_ctx *context, const char *path, char **buffer, size_t *length);

/**
  * The value of @path in @context is set to the data at @value with @length size
  */
void cvar_string_set(struct cvar_ctx *context, const char *path, const char *value, size_t length);

/**
  * Saves the @context to the file at @file_path
  */
bool cvar_file_save(struct cvar_ctx *context, const char *file_path);

/**
  * Imports the file at @file_path into the @context
  */
bool cvar_file_load(struct cvar_ctx *context, const char *file_path);

