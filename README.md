# Cvar

Cvar enables you to quickly create sets of named variables that can be exported/imported and stored to files.

  - Written in C99 to be available everywhere
  - O(n) lookup time for variables where n is length of variable length
  - No global states, everything is handled by contexts
  - Contexts can be exported, imported and saved to files
  - Saving to / loading from file makes a simple settings engine
  - Exporting context and sending the result makes a powerful messaging format
  - Uses strict sizes for variables (int64_t, int32_t) instead of platform dependent ones.

Things not yet implemented:

  - Endianness (integers from little-endian platforms will be reversed if sent to big-endian platforms)
  - Other dictionaries. The current dictionary uses about 2KiB of memory (256 possible character*8 byte pointer size) per character of the variable name). Other dictionaries are planned to trade higher lookup time for lower memory usage.
  - Custom character sets for dictionary. Currently all characters are supported for variable names, if only a subset of characters is required memory usage could be lowered.

## Documentation
Doxygen-style documentation in include/cvar.h

## Building from source

###All Platforms
```sh
$ git clone https://bitbucket.org/trippler/cvar
$ cd binary-visualizer/cvar
$ make
```
### Example code
```C
//Only header required by Cvar
#include <cvar.h>
#include <stdlib.h> //atexit
#include <stdint.h> //int64_t
#include <stdio.h> //printf
#include <inttypes.h> //PRId64

struct cvar_ctx *context;

//Function called by atexit to save and destroy the context
void save_context(void){
	//First save the entire context to file
	cvar_file_save(context, "variables.cvar");
	//Then destroy it
	cvar_ctx_destroy(context);
}

int main(){
	//Create a new context
	context = cvar_ctx_create();
	//Load file into cvar
	cvar_file_load(context, "variables.cvar");
	//When the application exits, save the context
	atexit(save_context);
	
	//Get the amount of executions
	int64_t executions = cvar_int64_get(context, "executions");
	
	printf("This program has been executed %"PRId64" times.\n", executions);
	
	//Save executions+1 as the new executions
	cvar_int64_set(context, "executions", executions+1);
}
```

###Output:
```sh
$ This program has been executed 0 times
$ This program has been executed 1 times
$ This program has been executed 2 times
```

License
----

GPL v2
