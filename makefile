CC=gcc
SOURCEDIR=src
SOURCES=cvar.c dict.c
OUTDIR=bin
FLAGS=--std=c99 -Wall -O2 -Iinclude -g

all: libcvar.a libcvar.so driver driver_static cleanup

cleanup: $(patsubst %.c, %.o, $(SOURCES)) $(patsubst %.c, %s.o, $(SOURCES))
	rm $^

clean:
	-rm $(OUTDIR)/libcvar.a $(OUTDIR)/libcvar.so

%.o: src/%.c
	$(CC) $(FLAGS) -o $@ -c $^

%s.o: src/%.c
	$(CC) $(FLAGS) -fPIC -o $@ -c $^

libcvar.a: $(patsubst %.c, %.o, $(SOURCES))
	ar rcs $(OUTDIR)/$@ $^

libcvar.so: $(patsubst %.c, %s.o, $(SOURCES))
	$(CC) -shared -Wl,-soname,$@ -o $(OUTDIR)/$@ $^

driver_static: libcvar.a
	$(CC) -static $(FLAGS) driver/main.c -Lbin -lcvar -o $(OUTDIR)/$@

driver: libcvar.so
	$(CC) $(FLAGS) driver/main.c -Lbin -lcvar -o $(OUTDIR)/$@
