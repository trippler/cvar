#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "cvar.h"
#include "types.h"

#define MIN(a,b) ((a)<(b))?(a):(b)

struct cvar_ctx* cvar_ctx_create(){
	struct cvar_ctx *context = (struct cvar_ctx*)calloc(1, sizeof(struct cvar_ctx));
	context->d = dict_create();
	return context;
}

void cvar_ctx_destroy(struct cvar_ctx *context){
	dict_destroy(context->d);
	free(context);
}

bool cvar_error(struct cvar_ctx *context){
	return context->error == 1;
}

void cvar_data_callback(struct cvar_ctx *context, 
		void (*callback_function)(const char*,void*,void*), 
		void* passthrough){
	dict_data_callback(context->d, callback_function, passthrough);
}

void cvar_generic_data_set(struct cvar_ctx *context, const char *path_prefix,
				const char *path, void *data){
	char *full_path = calloc(strlen(path_prefix)+strlen(path)+1, 1);
	sprintf(full_path, "%s%s", path_prefix, path);
	dict_data_set(context->d, full_path, data);
	free(full_path);
}

void* cvar_generic_data_get(struct cvar_ctx *context, const char *path_prefix,
				const char *path, enum cvar_type type){
	char *full_path = calloc(strlen(path_prefix)+strlen(path)+1, 1);
	sprintf(full_path, "%s%s", path_prefix, path);
	void *data = dict_data_get(context->d, full_path);
	free(full_path);
	if(!data || ((struct cvar_endpoint*)data)->type != type){
		context->error = 1;
		return NULL;
	}
	context->error = 0;
	return data;
}

void cvar_int64_set(struct cvar_ctx *context, const char *path, int64_t value){
	struct cvar_endpoint_int64 *ep = calloc(1, sizeof(struct cvar_endpoint_int64));
	ep->parent.type = INT64;
	ep->value = value;
	cvar_generic_data_set(context, "INT64_", path, ep);
	context->error = 0;
}

void cvar_int32_set(struct cvar_ctx *context, const char *path, int32_t value){
	struct cvar_endpoint_int32 *ep = calloc(1, sizeof(struct cvar_endpoint_int32));
	ep->parent.type = INT32;
	ep->value = value;
	cvar_generic_data_set(context, "INT32_", path, ep);
	context->error = 0;
}


void cvar_string_set(struct cvar_ctx *context, const char *path, const char *value, size_t length){
	int64_t total_length = sizeof(struct cvar_endpoint_string) + length;
	struct cvar_endpoint_string *ep = calloc(1, total_length);
	ep->parent.type = STRING;
	ep->length = total_length;
	memcpy(ep->data, value, length);
	cvar_generic_data_set(context, "STR_", path, ep);
	context->error = 0;
}

char* cvar_string_get(struct cvar_ctx *context, const char *path, char **buffer, size_t *length){
	struct cvar_endpoint_string *ep = cvar_generic_data_get(context, "STR_", path, STRING);
	if(!ep)
		return NULL;
	if(*buffer == 0){
		*buffer = (char*)calloc(1, ep->length + 1 - sizeof(struct cvar_endpoint_string));
		if(!*buffer){
			context->error = 1;
			return 0;
		}
		*length = ep->length - sizeof(struct cvar_endpoint_string);
	}
	*length = MIN(*length, ep->length - sizeof(struct cvar_endpoint_string));
	return memcpy(*buffer, ep->data, *length);
}

int64_t cvar_int64_get(struct cvar_ctx *context, const char *path){
	struct cvar_endpoint_int64 *ep = cvar_generic_data_get(context, "INT64_", path, INT64);
	if(!ep)
		return 0;
	return ep->value;
}

int32_t cvar_int32_get(struct cvar_ctx *context, const char *path){
	struct cvar_endpoint_int32 *ep = cvar_generic_data_get(context, "INT32_", path, INT32);
	if(!ep)
		return 0;
	return ep->value;
}


int64_t cvar_element_length(const void *data){
	int64_t length = -1;
	switch(((struct cvar_endpoint*)data)->type){
		case INT64:
			return sizeof(struct cvar_endpoint_int64);
		case INT32:
			return sizeof(struct cvar_endpoint_int32);
		case STRING:
			length = ((struct cvar_endpoint_string*)data)->length;
			if(length >= sizeof(struct cvar_endpoint_string))
				return length;
		default:
			return -1;
	}
}

void cvar_element_buffer_create(struct cvar_ctx *context, const char *path,
				char **buffer_out, int64_t *length){
	void *data = dict_data_get(context->d, path);
	if(!data)
		goto error;
	size_t name_length = strlen(path);
	int position = 0;
	*length = name_length + 1; /*Name and null*/
	*buffer_out = (char*)calloc(1, *length);
	if(!*buffer_out)
		goto error;
	/*Copy name and \0*/
	memcpy(*buffer_out, path, name_length);
	position = name_length + 1; //Past name+\0
	//Resize for data
	int64_t data_length = cvar_element_length(data);
	if(data_length < 0){
		free(*buffer_out);
		goto error;
	}
	*length += data_length;
	char *buffer = realloc(*buffer_out, *length);
	if(!buffer){
		free(*buffer_out);
		*buffer_out = 0;
		*length = 0;
		goto error;
	}
	*buffer_out = buffer;
	//Copy data
	memcpy((*buffer_out)+position, data, data_length);
	return;
error:
	context->error = 1;
	return;
}
/**
  * Create an element and add it to the context from a buffer
  * @Returns the amount of bytes used in the buffer
  */
int64_t cvar_buffer_element_create(struct cvar_ctx *context, const char *buffer, size_t max_length){
	size_t name_length = strlen(buffer);
	char *name = 0;
	if(name_length >= max_length)
		goto error;
	name = (char*)calloc(1, name_length+1);
	if(!name)
		goto error;
	memcpy(name, buffer, name_length);
	int64_t element_length = cvar_element_length(buffer+name_length+1);
	if(element_length >= max_length || element_length <= 0)
		goto error;
	void *data = calloc(1, element_length);
	if(!data)
		goto error;
	memcpy(data, buffer+name_length+1, element_length);
	if(!dict_data_set(context->d, name, data)){
		free(data);
		goto error;
	}
	free(name);
	context->error = 0;
	return element_length+name_length+1;
error:
	free(name);
	context->error = 1;
	return -1;
}

struct cvar_export_rec_data{
	char **buffer;
	int64_t *length;
	struct cvar_ctx *context;
};

void cvar_ctx_buffer_export_rec(const char *name, void *data, void *passthrough){
	struct cvar_export_rec_data *ed = passthrough;
	char *element = 0;
	int64_t length = 0;
	cvar_element_buffer_create(ed->context, name, &element, &length);
	*(ed->length) += length;
	*(ed->buffer) = realloc(*(ed->buffer), *(ed->length));
	memcpy(*(ed->buffer) + (*ed->length) - length, element, length);
	free(element);
}

void cvar_ctx_buffer_export(struct cvar_ctx *context, char **buffer, int64_t *length){
	*buffer = 0;
	*length = 0;
	struct cvar_export_rec_data ed = {
		.buffer = buffer,
		.length = length,
		.context = context
	};
	cvar_data_callback(context, cvar_ctx_buffer_export_rec, &ed);
}

void cvar_ctx_buffer_import(struct cvar_ctx *context, const char *buffer, int64_t length){
	int64_t used = 0;
	while(used < length){
		int64_t status = cvar_buffer_element_create(context, buffer+used, length-used);
		if(cvar_error(context))
			return;
		used += status;
	}
	context->error = 0;
	return;
}

struct cvar_file_rec_data{
	FILE *fp;
	struct cvar_ctx *context;
	bool error;
};

void cvar_file_save_rec(const char *name, void *data, void *passthrough){
	struct cvar_file_rec_data *fd = passthrough;
	char *element = 0;
	int64_t length = 0;
	cvar_element_buffer_create(fd->context, name, &element, &length);
	int64_t written = fwrite(element, sizeof(char), length, fd->fp);
	if(written != length)
		fd->error = true;
	free(element);
}
bool cvar_file_save(struct cvar_ctx *context, const char *file_path){
	FILE *fp = fopen(file_path, "wb");
	if(!fp)
		return false;
	struct cvar_file_rec_data fd = {
		.fp = fp,
		.context = context,
		.error = false
	};
	cvar_data_callback(context, cvar_file_save_rec, &fd);
	fclose(fp);
	return fd.error;
}

bool cvar_file_load(struct cvar_ctx *context, const char *file_path){
	FILE *fp = fopen(file_path, "rb");
	if(!fp)
		goto error_all;
	fseek(fp, 0, SEEK_END);
	int64_t size = ftell(fp);
	rewind(fp);
	char *buffer = calloc(size, sizeof(char));
	if(!buffer)
		goto error_memory;
	int64_t read_bytes = fread(buffer, sizeof(char), size, fp);
	cvar_ctx_buffer_import(context, buffer, read_bytes);
	free(buffer);
	fclose(fp);
	return true;
error_memory:
	fclose(fp);	
error_all:
	return false;
}

