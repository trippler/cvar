#include <stdlib.h> //calloc
#include "dict.h"

//Char to array-index
int ctoa(unsigned char c){
	return c;
}

struct dict* dict_create(){
	return (struct dict*)calloc(1, sizeof(struct dict));
}

void dict_destroy(struct dict *d){
	if(!d)
		return;
	for(int n = 0; n < cvar_letter_range; ++n)
		dict_destroy(d->subs[n]);
	free(d->data);
	free(d);
}

struct dict* dict_path_create(struct dict* d, const char *path){
	if(ctoa(path[0]) == 0 || !d)
		return d;
	if(!d->subs[ctoa(path[0])])
		d->subs[ctoa(path[0])] = dict_create();
	return dict_path_create(d->subs[ctoa(path[0])], path+1);
}

struct dict* dict_path_find(struct dict* d, const char *path){
	if(ctoa(path[0]) == 0)
		return d;
	if(!d->subs[ctoa(path[0])])
		return NULL;
	return dict_path_find(d->subs[ctoa(path[0])], path+1);
}

bool dict_data_set(struct dict *d, const char *path, void* data){
	struct dict *cur = dict_path_create(d, path);
	if(!cur)
		return false;
	cur->data = data;
	return true;
}

void* dict_data_get(struct dict *d, const char *path){
	struct dict *r = dict_path_find(d, path);
	if(r)
		return r->data;
	return NULL;
}

struct dict_callback_data{
	char *name;
	long long size,at;
	void (*callback_function)(const char*, void*, void*);
	void *passthrough;
};

void dict_internal_callback_rec(struct dict *d, struct dict_callback_data *cb_data){
	if(!d)
		return;
	if(d->data)
		cb_data->callback_function(cb_data->name, d->data, cb_data->passthrough);
	cb_data->at++;
	if(cb_data->at >= cb_data->size){
		cb_data->size *= 1.6; // 3/2 geometric series
		cb_data->name = realloc(cb_data->name, cb_data->size);
	}
	cb_data->name[cb_data->at] = 0;
	for(int n = 0; n < cvar_letter_range; ++n){
		cb_data->name[cb_data->at - 1] = n;
		dict_internal_callback_rec(d->subs[n], cb_data);
	}
	cb_data->at--;
	cb_data->name[cb_data->at] = 0;
}

void dict_data_callback(struct dict *d, 
		void (*callback_function)(const char*,void*,void*), 
		void *passthrough){
	struct dict_callback_data cb = {
		.name = (char*)malloc(4),
		.size = 4,
		.at = 0,
		.callback_function = callback_function,
		.passthrough = passthrough
	};
	dict_internal_callback_rec(d, &cb);
	free(cb.name);
}

